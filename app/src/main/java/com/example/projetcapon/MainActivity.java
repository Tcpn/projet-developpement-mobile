package com.example.projetcapon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.ListView;
import android.content.Context;
import android.view.MenuInflater;
import android.widget.ArrayAdapter;

import com.example.projetcapon.entities.Question;
import com.example.projetcapon.entities.Quiz;

import java.io.Serializable;
import java.util.Objects;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Quiz> quizList;

    private ListView lv;
    private ActivityResultLauncher<Intent> quizzLauncher;
    private ActivityResultLauncher<Intent> createQuizzLauncher;

    private ActivityResultLauncher<Intent> checkPasswordLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.quizList = new ArrayList<>();
        this.lv = findViewById(R.id.main_list);

        // On charge les quizs du stockage interne
        this.load();

        // Si le stockage ne contient aucune donnée, on charge les quizs prédéfinis
        if (this.quizList.size() == 0){
            this.loadInitialDatas();
        }

        // Affichage des thèmes
        ArrayList<String> quizLabelList = new ArrayList<>();
        this.quizList.forEach(quizElement -> quizLabelList.add(quizElement.getTheme()));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, quizLabelList);
        this.lv.setAdapter(adapter);

        // Si le retour d'activité de quiz est valide, on met à jour les scores
        this.quizzLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getData().hasExtra("theme")){
                    String theme = result.getData().getStringExtra("theme");
                    for (int i = 0; i < this.quizList.size(); i++){
                        if (Objects.equals(this.quizList.get(i).getTheme(), theme)){
                            int score = result.getData().getIntExtra("score", 0);
                            this.quizList.get(i).setScore(score);
                            this.quizList.get(i).setCompleted(true);
                        }
                    }
                }
            }
        );

        // Si le retour d'activité de connexion admin est valide, on lance l'activité de création de quiz
        this.checkPasswordLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK){
                        Intent intent = new Intent(this, CreateQuizActivity.class);
                        this.createQuizzLauncher.launch(intent);
                    }
                }
        );

        // Si le retour de l'activité de création d'activité est valide, on ajout le quiz créé à la liste
        this.createQuizzLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK && result.getData().hasExtra("quiz")){
                        Quiz newQuiz = (Quiz) result.getData().getSerializableExtra("quiz");
                        this.quizList.add(newQuiz);
                        quizLabelList.add(newQuiz.getTheme());
                        adapter.notifyDataSetChanged();
                    }
                }
        );

        setSupportActionBar(findViewById(R.id.main_toolbar));
    }

    /**
     * Chargement des données enregistrées
     */
    private void load(){
        try {
            FileInputStream fileInputStream = openFileInput("datas.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            this.quizList = (ArrayList<Quiz>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Chargement des données prédéfinies
     */
    private void loadInitialDatas(){
        InputStream inputStream;

        try {
            inputStream = getAssets().open("Quiz.txt");

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String ligne;

            while ((ligne = reader.readLine()) != null) {
                stringBuilder.append(ligne).append("\n");
            }
            inputStream.close();

            String[] lines = stringBuilder.toString().split("\\r?\\n");
            int i = 0;

            while(!lines[i].contains("END")){
                Quiz quiz = new Quiz();
                quiz.setTheme(lines[i]);
                i++;

                while(!lines[i].contains("NEXT") && !lines[i].contains("END")){
                    Question question = new Question(lines[i]);
                    for (int j = 0; j < 3; j++){
                        i++;
                        String answer = lines[i];
                        if (lines[i].endsWith("x")){
                            question.setRightAnswer(j);
                            answer = lines[i].substring(0, lines[i].length() - 1);
                            answer = answer + "";
                        }
                        question.addAnswer(answer);
                    }
                    quiz.addQuestion(question);
                    i++;
                }
                this.quizList.add(quiz);
                if (lines[i].contains("NEXT")){
                    i++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sauvegarde des quizs dans le stockage interne
     */
    private void save(){
        try {
            FileOutputStream fileOutputStream = openFileOutput("datas.txt", Context.MODE_PRIVATE);;
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(this.quizList);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Accès à l'activité de quiz selon le quiz séléctionné
     * @param view
     */
    public void accessQuizz(View view){
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) this.lv.getAdapter();
        Quiz quiz = null;

        // Vérification du quiz séléctionné
        for (int i = 0; i < adapter.getCount(); i++) {
            if (this.lv.isItemChecked(i)) {
                quiz = this.quizList.get(i);
            }
        }

        // Si aucun quiz séléctionné message d'erreur
        if (quiz == null){
            Toast.makeText(this, "Veuillez sélectionner un thème", Toast.LENGTH_SHORT).show();
        }

        // Si le quiz est déjà complété message d'erreur
        else if(quiz.isCompleted()){
            Toast.makeText(this, "Ce quiz est déjà complété", Toast.LENGTH_SHORT).show();
        }

        // Sinon accès à l'activité de quiz
        else{
            Intent intention = new Intent(this, QuizActivity.class);
            intention.putExtra("quiz", (Serializable) quiz);
            this.quizzLauncher.launch(intention);
        }
    }

    //********************** MENU *******************//

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_view_score){
            this.viewScore();
        }
        else if (id == R.id.menu_reset_score) {
            this.resetScore();
        }
        else if (id == R.id.menu_create_quiz) {
            this.createQuiz();
        }
        else if (id == R.id.menu_exit) {
            this.exit();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Accès à l'activité de visualisation des scores
     */
    public void viewScore(){
        Intent i = new Intent(this, ViewScoreActivity.class);
        i.putExtra("quizList", this.quizList);
        startActivity(i);
    }

    /**
     * Remise à zero des scores de quizs
     */
    public void resetScore(){
        for (Quiz quiz : this.quizList){
            quiz.setScore(0);
            quiz.setCompleted(false);
        }
        Toast.makeText(this, "Scores réinitialisés !", Toast.LENGTH_SHORT).show();
    }

    /**
     * Accès à l'activité de quiz
     */
    public void createQuiz(){
        Intent i = new Intent(this, LoginActivity.class);
        this.checkPasswordLauncher.launch(i);
    }

    /**
     * Sauvegarde des données et sortie de l'application
     */
    public void exit(){
        this.save();
        finish();
    }
}