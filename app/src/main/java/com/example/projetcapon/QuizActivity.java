package com.example.projetcapon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import java.util.Collections;
import android.widget.TextView;
import android.widget.RadioGroup;
import android.widget.RadioButton;

import com.example.projetcapon.entities.Question;
import com.example.projetcapon.entities.Quiz;

public class QuizActivity extends AppCompatActivity {

    private Quiz quiz;
    private int number;
    private RadioGroup rgp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        this.rgp = findViewById(R.id.quiz_radio_group);

        // Récupération du quiz et mélange des questions
        this.quiz = (Quiz) getIntent().getSerializableExtra("quiz");
        Collections.shuffle(this.quiz.getQuestionList());

        // Affichage du thème
        TextView theme = findViewById(R.id.quiz_theme);
        theme.setText(quiz.getTheme());

        // Affichage de la première question
        this.number = -1;
        this.nextQuestion();
    }

    /**
     * Passage à la prochaine question ou retour à l'activité principale
     */
    public void submit(View view){
        int selectedId = this.rgp.getCheckedRadioButtonId();

        // Si aucune réponse séléctionnée
        if (selectedId == -1){
            Toast.makeText(this, "Veuillez sélectionner une réponse", Toast.LENGTH_SHORT).show();
        }
        else{
            // Si bonne réponse
            if (this.isRightAnswer(selectedId)){
                Toast.makeText(this, "Bonne réponse !", Toast.LENGTH_SHORT).show();
                this.quiz.setScore(this.quiz.getScore() + 1);
            }
            // Si mauvaise réponse
            else{
                Toast.makeText(this, "Mauvaise réponse ...", Toast.LENGTH_SHORT).show();
            }

            // S'il reste des question passage à la prochaine
            if (this.number < this.quiz.getQuestionList().size() - 1){
                this.nextQuestion();
            }
            // Sinon envoi des données à l'activité principale
            else{
                getIntent().putExtra("theme", this.quiz.getTheme());
                getIntent().putExtra("score", this.quiz.getScore());
                setResult(RESULT_OK, getIntent());
                finish();
            }
        }
    }

    /**
     * Annulation du quiz et retour à l'activité principale
     */
    public void cancel(View view){
        setResult(RESULT_CANCELED, getIntent());
        getIntent().putExtra("theme", this.quiz.getTheme());
        finish();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Passage à la prochaine question
     */
    private void nextQuestion(){
        this.number ++;
        Question question = this.quiz.getQuestionList().get(this.number);

        // Affichage du numéro de question
        String numberDisplay = "Question " + (this.number + 1) + "/" + quiz.getQuestionList().size();
        TextView questionNumber = findViewById(R.id.quiz_number);
        questionNumber.setText(numberDisplay);

        // Affichage de la question
        TextView questionDisplay = findViewById(R.id.quiz_question);
        questionDisplay.setText(question.getLabel());

        // Suppression des anciennes réponses
        this.rgp.clearCheck();
        this.rgp.removeAllViews();

        // Ajout des nouvelles réponses
        RadioGroup.LayoutParams rprms;
        for(int i=0; i < question.getAnswerList().size(); i++){
            RadioButton newRadioButton = new RadioButton(this);
            newRadioButton.setText(question.getAnswerList().get(i));
            newRadioButton.setId(View.generateViewId());
            rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            this.rgp.addView(newRadioButton, rprms);
        }
    }

    /**
     * Vérifie si la réponse choisie est valide
     * @param selectedId id du bouton radio séléctionné
     * @return true: bonne réponse | false: mauvaise réponse
     */
    private boolean isRightAnswer(int selectedId){
        RadioButton radioButton = findViewById(selectedId);
        Question question = this.quiz.getQuestionList().get(this.number);

        for (int i = 0; i < question.getAnswerList().size(); i++){
            if (radioButton.getText() == question.getAnswerList().get(i)){
                if (question.getRightAnswer() == i){
                    return true;
                }
            }
        }
        return false;
    }
}