package com.example.projetcapon;

import android.os.Bundle;
import android.view.View;
import java.util.ArrayList;
import android.view.Gravity;

import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TableLayout;

import com.example.projetcapon.entities.Quiz;
import androidx.appcompat.app.AppCompatActivity;

public class ViewScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_score);

        ArrayList<Quiz> quizList = (ArrayList<Quiz>) getIntent().getSerializableExtra("quizList");
        TableLayout layout = findViewById(R.id.view_score_layout);
        int averageScoreCpt = 0;

        // Ajout des scores pour chaque thème
        for (int i = 0; i < quizList.size(); i++){
            Quiz quiz = quizList.get(i);
            TableRow row = new TableRow(this);

            TextView theme = new TextView(this);
            String themeLabel = "Notes questionnaire " + quiz.getTheme();
            theme.setText(themeLabel);
            theme.setGravity(Gravity.START);

            TextView score = new TextView(this);
            String scoreLabel = (quiz.isCompleted() ? quiz.getScore() : "Non complété ") + "/" + quiz.getQuestionList().size();
            score.setText(scoreLabel);
            score.setGravity(Gravity.END);

            row.addView(theme);
            row.addView(score);

            layout.addView(row);

            averageScoreCpt += 20 * quiz.getScore() / quiz.getQuestionList().size();
        }

        // Ajout du score moyen
        TableRow row = new TableRow(this);

        TextView averageScoreLabel = new TextView(this);
        averageScoreLabel.setText("Note moyenne");
        averageScoreLabel.setGravity(Gravity.START);

        TextView averageScore = new TextView(this);
        String averageScoreText = Float.toString(averageScoreCpt / quizList.size()) + "/20";
        averageScore.setText(averageScoreText);
        averageScore.setGravity(Gravity.END);

        row.addView(averageScoreLabel);
        row.addView(averageScore);

        layout.addView(row);
    }

    /**
     * Retour à l'activité principale
     */
    public void exit(View view){
        finish();
    }
}