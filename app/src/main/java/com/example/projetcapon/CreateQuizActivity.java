package com.example.projetcapon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import android.widget.LinearLayout;

import com.example.projetcapon.entities.Quiz;
import com.example.projetcapon.entities.Question;

public class CreateQuizActivity extends AppCompatActivity {
    private Quiz quiz;
    private EditText questionInput;
    private LinearLayout answerLayout;
    private RadioGroup answerRadioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz);
        this.quiz = new Quiz();
        this.questionInput = findViewById(R.id.create_quiz_input_question);
        this.answerLayout = findViewById(R.id.create_quizz_layout_answer);
        this.answerRadioGroup = findViewById(R.id.create_quizz_radio_answer);
    }

    /**
     * Retour à l'activité principale
     */
    public void cancel(View view){
        setResult(RESULT_CANCELED, getIntent());
        finish();
    }

    /**
     * Envoi du quiz à l'activité principale
     */
    public void submit (View view){
        boolean hasError = false;
        EditText themeInput = findViewById(R.id.create_quiz_theme_input);

        // Si le thème n'est pas renseigné
        if (themeInput.getText().toString().equals("")){
            Toast.makeText(this, "Le thème ne peut être vide.", Toast.LENGTH_SHORT).show();
            hasError = true;
        }

        // Si aucune question n'a été renseignée
        if (this.quiz.getQuestionList().size() == 0){
            Toast.makeText(this, "Veuillez ajouter au moins une question.", Toast.LENGTH_SHORT).show();
            hasError = true;
        }

        // S'il n'y a aucune erreur
        if (!hasError) {
            this.quiz.setTheme(themeInput.getText().toString());
            getIntent().putExtra("quiz", quiz);
            setResult(RESULT_OK, getIntent());
            finish();
        }
    }

    /**
     * Ajout d'une question si les conditions sont remplies
     */
    public void addQuestion(View view){
        String label = this.questionInput.getText().toString();

        // Si l'intitulé de la question est vide
        if (label.equals("")){
            Toast.makeText(this, "L'intitulé ne peut être vide.", Toast.LENGTH_SHORT).show();
        }
        else{
            Question question = this.createQuestion(label);

            // Si les réponses ou les checkbox ne sont pas valides
            if (question == null){
                Toast.makeText(this, "Veuillez remplir les réponses.", Toast.LENGTH_SHORT).show();
            }
            else{
                // Ajout de la question et mise à jour de l'affichage
                this.quiz.addQuestion(question);
                this.updateDisplay();

                Toast.makeText(this, "Question ajoutée !", Toast.LENGTH_SHORT).show();
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Crée une entité question si les champs sont valides
     * @return Question|null
     */
    private Question createQuestion(String label){
        Question question = new Question(label);

        boolean isQuestionValid = true;
        boolean isCheckBoxValid = false;

        for (int i = 0; i < this.answerLayout.getChildCount(); i++){
            TextView answer = (TextView)this.answerLayout.getChildAt(i);
            if (answer.getText().toString().equals("")){
                isQuestionValid = false;
            }
            question.addAnswer(answer.getText().toString());

            RadioButton button = (RadioButton)this.answerRadioGroup.getChildAt(i);
            if (button.getId() == this.answerRadioGroup.getCheckedRadioButtonId()){
                question.setRightAnswer(i);
                isCheckBoxValid = true;
            }
        }

        return (isQuestionValid && isCheckBoxValid) ? question : null;
    }

    /**
     * Met à jour l'affichage
     */
    private void updateDisplay(){
        this.questionInput.setText("");
        this.answerRadioGroup.clearCheck();

        for (int i = 0; i < this.answerLayout.getChildCount(); i++){
            TextView answer = (TextView)this.answerLayout.getChildAt(i);
            answer.setText("");
        }

        String questionLabelText = "Question " + (this.quiz.getQuestionList().size() + 1);
        TextView questionLabel = findViewById(R.id.create_quiz_label_question);
        questionLabel.setText(questionLabelText);
    }
}