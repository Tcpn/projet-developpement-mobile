package com.example.projetcapon;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    /**
     * Envoi des données à l'activité principale si mot de passe valide
     */
    public void submit(View view){
        EditText loginInput = findViewById(R.id.login_input);

        if (!loginInput.getText().toString().equals("MDP")){
            Toast.makeText(this, "Mot de passe invalide !", Toast.LENGTH_SHORT).show();
        }
        else{
            setResult(RESULT_OK, getIntent());
            finish();
        }
    }

    /**
     * Retour à l'activité principale
     */
    public void cancel(View view){
        setResult(RESULT_CANCELED, getIntent());
        finish();
    }
}