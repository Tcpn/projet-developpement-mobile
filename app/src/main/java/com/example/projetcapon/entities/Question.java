package com.example.projetcapon.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class Question implements Serializable {

    private String label;

    private ArrayList<String> answerList;

    private int rightAnswer;

    public Question(String label) {
        this.label = label;
        this.answerList = new ArrayList<>();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ArrayList<String> getAnswerList() {
        return answerList;
    }

    public void addAnswer(String answer){
        this.answerList.add(answer);
    }

    public int getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(int rightAnswer) {
        this.rightAnswer = rightAnswer;
    }
}
