package com.example.projetcapon.entities;

import java.util.ArrayList;
import java.io.Serializable;

public class Quiz implements Serializable {
    private ArrayList<Question> questionList;
    private String theme;
    private int score;
    private boolean isCompleted;

    public Quiz() {
        this.questionList = new ArrayList<>();
        this.score = 0;
        this.isCompleted = false;
    }

    public ArrayList<Question> getQuestionList() {
        return questionList;
    }

    public void addQuestion(Question question) {
        this.questionList.add(question);
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }
}
